# Build image using
# sudo docker build --pull --rm -f "Dockerfile" -t socat:1.0.0 "."
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y iputils-ping
RUN apt-get install -y socat
RUN apt-get install -y telnet
RUN apt-get install -y iproute2