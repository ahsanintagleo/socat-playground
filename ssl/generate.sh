# Guide
# https://robyoung.digital/2020/01/15/ssl-certificate-authority-with-openssl-part-two/

DOMAIN_NAME=$1

if [ -z "$DOMAIN_NAME" ] 
then DOMAIN_NAME="127.0.0.1"
fi

PASSWORD="123456"

echo "DOMAIN_NAME=$DOMAIN_NAME"

openssl genrsa -aes256 -passout pass:123456 -out test_ca.key 2048
openssl req -new -key test_ca.key -x509 -days 1000 -out test_ca.crt \
  -passin pass:123456 \
  -subj "/C=GB/O=robyoung.digital/CN=test" \
    

openssl req -x509 -new -nodes \
  -passin pass:123456 \
  -key test_ca.key \
  -sha256 -days 1825 \
  -subj "/C=GB/O=robyoung.digital/CN=test" \
  -out test_ca.pem

#########################################################################

openssl genrsa -out test_server.key 2048

openssl req -new \
  -subj "/C=GB/CN=$DOMAIN_NAME" \
  -key test_server.key \
  -out test_server_static_csr.pem

openssl x509 \
  -req -in test_server_static_csr.pem \
  -passin pass:123456 \
  -CA test_ca.pem -CAkey test_ca.key -CAcreateserial \
  -out test_server.crt \
  -days 1825 -sha256 \
  -extfile config_server.cnf

#########################################################################

openssl genrsa -out test_client.key 2048

openssl req -new \
  -subj "/C=GB" \
  -key test_client.key \
  -out test_client.csr

openssl x509 \
  -req -in test_client.csr \
  -passin pass:123456 \
  -CA test_ca.pem -CAkey test_ca.key -CAcreateserial \
  -out test_client.crt \
  -days 1825 -sha256

openssl pkcs12 -export -clcerts \
  -in test_client.crt -inkey test_client.key -certfile test_ca.crt -out test_client.p12 \
  -name "client" -caname "authority"

cat test_client.key test_client.crt test_ca.pem > test_client.pem