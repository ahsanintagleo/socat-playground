# SOCAT - Test env (Using Doker)

## With TLS
### Usage

Open terminal, navigate to the ssl_key folder & execute the command.


```bash
sh generate.sh 172.30.0.2
```

...move back to the root folder & build image using the command

```bash
docker build --pull --rm -f "Dockerfile" -t socat:1.0.0 "."
```

Use docker-compose to start containers

```bash
docker-compose up

# OR

docker-compose up && docker-compose down # To remove containers upon termination
```
Open two separate terminals & execute the following commands:
```bash
docker exec -it socat_server telnet 127.0.0.1 5051

# AND

docker exec -it socat_client telnet 127.0.0.1 5051
```

Alternatively, for a recursive reconnect approach.
```bash
while (true); do sleep 4; clear; docker exec -it socat_server telnet 127.0.0.1 5051; done;

# AND

while (true); do sleep 4; clear; docker exec -it socat_client telnet 127.0.0.1 5051; done;
```